
* `kubectl`, [Install instructions](https://kubernetes.io/docs/tasks/tools/#kubectl)
* `helm`, [Install instructions](https://helm.sh/docs/intro/install/)
* `commodore`, [Install instructions](https://syn.tools/commodore/explanation/running-commodore.html)
