Create the following environment variables and files.

```shell title="Environment Variables"
export TENANT_ID="t-my-tenant"
```

1. Create a file named `tenant.json`.
```shell
cat <<EOF > tenant.json
{
  "id": ${CLUSTER_ID},
  "tenant": ${TENANT_ID},
  "displayName": "My Beautifull vCluster",
  "facts": {
    "cloud": "vcluster",
    "distribution": "k3s",
  },
}
EOF
```

1. Add the tenant to Lieutenant.
```shell
lt_tenant -X POST --data @tenant.json
```
