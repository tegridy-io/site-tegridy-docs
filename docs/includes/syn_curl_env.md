To interact with the Lieutenant API create the following environment variables and aliases.

```shell title="Environment Variables"
export LIEUTENANT_API_URL="https://syn.example.com"
export LIEUTENANT_AUTH="Authorization: Bearer $(commodore fetch-token)"
```

```shell title="Aliases"
alias lt_tenant='curl -H "${LIEUTENANT_AUTH}" "${LIEUTENANT_API_URL}/tenants"'
alias lt_cluster='curl -H "${LIEUTENANT_AUTH}" "${LIEUTENANT_API_URL}/clusters"'
```
