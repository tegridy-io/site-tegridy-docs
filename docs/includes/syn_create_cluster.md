Create the following environment variables and files.

```shell title="Environment Variables"
export CLUSTER_ID="c-my-cluster"
export TENANT_ID="t-my-tenant"
```

1. Create a file named `cluster.json`.
```shell
cat <<EOF > cluster.json
{
  "id": ${CLUSTER_ID},
  "tenant": ${TENANT_ID},
  "displayName": "My Beautifull vCluster",
  "facts": {
    "cloud": "vcluster",
    "distribution": "k3s",
  },
}
EOF
```

1. Add the cluster to Lieutenant.
```shell
lt_cluster -X POST --data @cluster.json
```
