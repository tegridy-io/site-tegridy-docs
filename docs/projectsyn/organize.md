---
title: Organize
hide:
  - path
---

This is an example on how you could organize a component installation. We may want create more than one lieutenant environment later, to handle this better we create a "package" for the lieutenant installation.

## Create a Lieutenant Package

!!! note
    We can later reference this package in the tenant repository. If you add this package to a second cluster (or use the multi tenancy feature of a component) you can then only adjust the needed values.

!!! note
    Also, we define only one Vault policy in the package, as the second one is individual anyway.

### `packages/lieutenant.yaml`

```yaml
applications:
  - lieutenant
  - vault

parameters:
  lieutenant:
    namespace: infra-lieutenant

    operator:
      default_global_git_repo: ssh://git@gitlab.com/your-org/commodore-defaults.git
      env:
        LIEUTENANT_CREATE_SERVICEACCOUNT_TOKEN_SECRET: true
      vault:
        addr: https://vault.your-org.com    # we leave this value in the "package" as defaults
        auth_path: lieutenant
        path: clusters/kv

    api:
      ingress:
        host: syn.your-org.com              # we leave this value in the "package" as defaults
        annotations:
          cert-manager.io/cluster-issuer: letsencrypt-prod

    auth_delegation:
      lieutenant-sa:
        apiGroup: rbac.authorization.k8s.io
        kind: Group
        name: system:serviceaccounts
        namespace: infra-lieutenant

    ## TODO: githosts

    tenant_template:
      # GitRepo template for cluster repo
      clusterTemplate:
        gitRepoTemplate:
          apiSecretRef:
            name: gitlab-com
          path: tegridy-io/syn
          repoName: cluster-{{ .Name }}
      # GitRepo template for tenant repo
      gitRepoTemplate:
        apiSecretRef:
          name: gitlab-com
        path: tegridy-io/syn
        repoName: tenant-{{ .Name }}

  vault:
    namespace: infra-vault

    ingress:
      host: vault.your-org.com              # we leave this value in the "package" as defaults
      annotations:
        cert-manager.io/cluster-issuer: letsencrypt-prod

    storage:
      size: 10G

    backup:
      password: ?{vaultkv:${cluster:tenant}/${cluster:name}/vault/backupPassword}
      bucket:
        name: ${cluster:name}-vault-backup
        accesskey: ?{vaultkv:${cluster:tenant}/${cluster:name}/aws/accessKey}
        secretkey: ?{vaultkv:${cluster:tenant}/${cluster:name}/aws/secretKey}

    config:
      policies:
        # Only the Lieutenant Operator policy
        - name: lieutenant-operator
          rules: |
            path "clusters/kv/data/*" {
              capabilities = ["read", "create", "update", "delete"]
            }
            path "clusters/kv/metadata/*" {
              capabilities = ["read", "create", "update", "delete", "list"]
            }
            path "clusters/kv/delete/*" {
              capabilities = ["update"]
            }
      auth:
        - type: kubernetes
          path: lieutenant
          roles:
            - name: syn-cluster
              bound_service_account_names: "*"
              bound_service_account_namespaces: infra-lieutenant
              policies: syn-cluster
              ttl: 1h
            - name: lieutenant-operator
              bound_service_account_names: lieutenant-operator
              bound_service_account_namespaces: infra-lieutenant
              policies: lieutenant-operator
              ttl: 1h
```


## Update Tenant Repository

Update your cluster configuration file in the tenant repository.

### `hilarious-tabby.yaml`

```yaml
classes:
  - tegridy-io.common
  - global.packages.lieutenant

parameters:
  lieutenant:
    operator:
      vault:
        addr: https://vault.your-org.com    # if you want a different url

    api:
      ingress:
        host: syn.your-org.com              # if you want a different url

  vault:
    storage:
      class: hcloud-volumes

    ingress:
      host: vault.your-org.com              # if you want a different url

    config:
      policies:
        - name: syn-cluster
          rules: | # Allow to get cluster's own secrets
            path "clusters/kv/data/+/{{identity.entity.aliases.auth_kubernetes_124c972e.metadata.service_account_name}}/*" {
              capabilities = ["read"]
            }
            path "clusters/kv/data/__shared__/__shared__/*" {
              capabilities = ["read"]
            }
```


## Disable Monitoring and Backup

In this how-to we don't have monitoring or backup configured, disable it for now:

### `hilarious-tabby.yaml`

```yaml
parameters:
  argocd:
    monitoring:
      enabled: false

  vault:
    backup:
      enabled: false
```
