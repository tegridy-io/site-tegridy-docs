---
title: Vault Integration
hide:
  - path
---

To be able to propperly manage secrets we need a secret store like Vault.


## Add Vault Component

Add the component to your global defaults repository.


### `components.yaml`

```yaml
parameters:
  components:
    ...
    vault:
      url: https://github.com/projectsyn/component-vault.git
      version: v1.3.2
```


## Vault Configuration

For all available options, look into the component documentation.

* [Component Vault Documentation](https://hub.syn.tools/vault/)


### Common Parameters

The following parameters should be trivial.

```yaml
parameters:
  vault:
    namespace: infra-vault

    ingress:
      annotations:
        cert-manager.io/cluster-issuer: letsencrypt-prod
      host: vault.your-org.com

    storage:
      size: 10G
```


### Backup Configuration

You can use Project K8up to backup your Vault data, here you can define the backup parameters.

!!! info
    In the example below the location in vault is dynamically calculated, for the cluster in this how-to the path would be:

    `<vault>/cluster/kv/tegridy-io/hilarious-tabby/vault/backupPassword` or

    `<vault>/cluster/kv/tegridy-io/hilarious-tabby/aws/accessKey`


```yaml
parameters:
  vault:
    backup:
      password: ?{vaultkv:${cluster:tenant}/${cluster:name}/vault/backupPassword}
      bucket:
        name: tegridy-${cluster:name}-vault-backup
        accesskey: ?{vaultkv:${cluster:tenant}/${cluster:name}/aws/accessKey}
        secretkey: ?{vaultkv:${cluster:tenant}/${cluster:name}/aws/secretKey}
```


### Vault Authentication

We need 2 roles for authenticatig to Vault. One for the Lieutenant Operator, who needs to access all clusters. And one for the indivudual cluster, who only can access its secrets.

Every cluster gets its own service account.

```yaml
parameters:
  vault:
      auth:
        - type: kubernetes
          path: lieutenant
          roles:
            - name: lieutenant-operator
              bound_service_account_names: lieutenant-operator
              bound_service_account_namespaces: infra-lieutenant
              policies: lieutenant-operator
              ttl: 1h
            - name: syn-cluster
              bound_service_account_names: "*"
              bound_service_account_namespaces: infra-lieutenant
              policies: syn-cluster
              ttl: 1h
```


### Vault Policies

To define the access priviliges for the different roles, we need to create 2 policies.

!!! info
    Note, that the variable path in the below example will change with every installation of Vault. The following expression:

    `auth_kubernetes_124c972e`

    nees to be adjusted to your environment.

    You can look up in your Vault instance under `Access` (top left) --> `Auth Methods`.

    * [Vault Auth Methods](https://developer.hashicorp.com/vault/docs/concepts/auth)

```yaml
parameters:
  vault:
    config:
      policies:
        - name: syn-cluster
          rules: | # Allow to get cluster's own secrets
            path "clusters/kv/data/+/{{identity.entity.aliases.auth_kubernetes_124c972e.metadata.service_account_name}}/*" {
              capabilities = ["read"]
            }
            path "clusters/kv/data/__shared__/__shared__/*" {
              capabilities = ["read"]
            }
        - name: lieutenant-operator
          rules: |
            path "clusters/kv/data/*" {
              capabilities = ["read", "create", "update", "delete"]
            }
            path "clusters/kv/metadata/*" {
              capabilities = ["read", "create", "update", "delete", "list"]
            }
            path "clusters/kv/delete/*" {
              capabilities = ["update"]
            }
```


## Update Global Defaults

Update the common parameters in the global defaults repository, so Commodore knows how to connect to Vault.

### `parameters.yaml`

```yaml
parameters:
  kapitan:
    vars:
      namespace: cluster
    secrets:
      vaultkv:
        VAULT_ADDR: ${secret_management:vault_addr}
        VAULT_SKIP_VERIFY: "false"
        VAULT_CAPATH: /etc/ssl/certs/
        auth: token
        engine: kv-v2
        mount: clusters/kv

  secret_management:
    vault_addr: https://vault.your-org.com
    vault_role: syn-cluster
    vault_auth_mount_path: auth/lieutenant
```


## Lieutenant Configuration

!!! danger
    Replace your Lieutenant configuration with these parameters.

```yaml
parameters:
  lieutenant:
    namespace: infra-lieutenant

    operator:
      default_global_git_repo: ssh://git@gitlab.com/tegridy-io/syn/commodore-defaults.git
      env:
        LIEUTENANT_CREATE_SERVICEACCOUNT_TOKEN_SECRET: true
      vault:
        addr: https://vault.your-org.com
        auth_path: lieutenant
        path: clusters/kv

    api:
      ingress:
        host: syn.your-org.com
        annotations:
          cert-manager.io/cluster-issuer: letsencrypt-prod

    auth_delegation:
      lieutenant-sa:
        apiGroup: rbac.authorization.k8s.io
        kind: Group
        name: system:serviceaccounts
        namespace: infra-lieutenant

    ## TODO: githosts

    tenant_template:
      # GitRepo template for cluster repo
      clusterTemplate:
        gitRepoTemplate:
          apiSecretRef:
            name: gitlab-com
          path: tegridy-io/syn
          repoName: cluster-{{ .Name }}
      # GitRepo template for tenant repo
      gitRepoTemplate:
        apiSecretRef:
          name: gitlab-com
        path: tegridy-io/syn
        repoName: tenant-{{ .Name }}
```
