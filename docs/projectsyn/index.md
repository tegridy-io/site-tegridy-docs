---
title: Project Syn
hide:
  - toc
---

# ![Image title](https://syn.tools/syn/_images/logo_projectsyn.svg){ width="300" valign=center }


> Project Syn is a set of tools helping to securely manage a fleet of Kubernetes clusters. It brings a hierarchical configuration management based on GitOps principles, reusable components and an inventory of information about all Kubernetes clusters.

* [Project Syn Homepage](https://syn.tools)
* [Component Hub](https://hub.syn.tools)

## Overview of Project Syn tools

* [Commodore](https://syn.tools/commodore/index.html), a hierarchical configuration management with Kapitan.
* [Lieutenant Operator](https://syn.tools/lieutenant-operator/index.html), a Kubernetes operator providing the business logic for the API.
* [Lieutenant API](https://syn.tools/lieutenant-api/index.html), a REST API for cluster management.
* [Steward](https://syn.tools/steward/index.html), an in-cluster agent.

## Concepts

* [Concepts of Project Syn](https://syn.tools/commodore/reference/concepts.html)


### Creating Objects in Lieutenants

``` mermaid
sequenceDiagram
  participant C as Commodore
  participant L as Lieutenant
  participant G as Git Repositories
  participant S as Steward
  participant T as Target Cluster

  C->>L: create tenant
  L->>G: create tenant repo

  C->>L: create cluster
  L->>G: create cluster repo
```

### Compile Cluster Catalog

``` mermaid
sequenceDiagram
  participant C as Commodore
  participant L as Lieutenant
  participant G as Git Repositories
  participant S as Steward
  participant T as Target Cluster

  C->>L: get cluster info
  L-->>C: cluster info

  G-->>C: get global defaults
  G-->>C: get tenant repo
  C->>C: compile
  C->>G: push to cluster repo

  loop
    G-->>S: get cluster repo
    S->>T: reconcile
  end
```
