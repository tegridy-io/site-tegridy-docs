---
title: Global Defaults
---

In this section we look at the global defaults repository and see how we can organise a bit better.

> The Kapitan inventory used by Commodore builds on top of reclass. The Reclass fork used by Kapitan allows to use references in class names. This allows to dynamically build up the configuration hierarchy.

## Inventory Hierarchy

!!! info
    The configuration hierarchy can be defined and adjusted freely to your needs. There are also some inventory parameters that can be used to define the hierarchy.

* [Inventory Hierarchy](https://syn.tools/commodore/reference/hierarchy.html)
* [Managed Inventory Parameters](https://syn.tools/commodore/reference/parameters.html)


### `commodore.yaml`

To establish a configuration hierarchy, edit the `commodore.yaml` file:
```
classes:
- global.components
- global.namespace
- global.parameters
- global.distribution.${facts:distribution}
- global.cloud.${facts:cloud}
- ${cluster:tenant}.${cluster:name}
```


## Defaults for all clusters

The following configurations are applied to all clusters.

### `components.yaml`

In this file we define the components version and location.

```yaml
parameters:
  components:
    argocd:
      url: https://github.com/projectsyn/component-argocd.git
      version: v5.4.0

    backup-k8up:
      url: https://github.com/projectsyn/component-backup-k8up.git
      version: v4.3.2

    csi-hetznercloud:
      url: https://github.com/tegridy-io/component-csi-hetznercloud.git
      version: v1.0.1

    lieutenant:
      url: https://github.com/projectsyn/component-lieutenant.git
      version: v2.11.3

    steward:
      url: https://github.com/projectsyn/component-steward.git
      version: v3.5.2

    storageclass:
      url: https://github.com/projectsyn/component-storageclass.git
      version: v1.0.0

    traefik:
      url: https://github.com/tegridy-io/component-traefik.git
      version: v1.0.0
```


### `parameters.yaml`

In this file we can configure default components and configurations.

```yaml
applications:
  - argocd
  - steward
  - traefik

parameters:
  steward:
    api_url: https://syn.your-org.com
```


### `namespace.yaml`

!!! info
    This is not strictly necessary, only if you wan't generally have different namespaces than defined upstream.

In this file we can define the components default namespace, if we don't like the upstream choice.

```yaml
parameters:
  argocd:
    namespace: efk-system

  csi_hetznercloud:
    namespace: efk-storage-hcloud

  steward:
    namespace: efk-system

  traefik:
    namespace: efk-ingress-traefik
```


## Distribution specific configurations

### `distribution/k3s.yaml`

In this file we configure any adjustments to K3s clusters.

```yaml
# empty
```

## Cloud specific configurations

### `cloud/hetzner.yaml`

In this file we can configure adjustments for the hetzner cloud.

```yaml
applications:
  - csi-hetznercloud
  - storageclass

parameters:
  storageclass:
    defaultClass: hcloud-volumes

  csi_hetznercloud:
    hcloudToken: ?{vaultkv:${cluster:tenant}/${cluster:name}/hcloud/token}
```
