# Setup vCluster

Instructions to setup a vCluster using the Helm chart.

## Prerequisites

{!syn_requiredtools.md!}


## Create Cluster in Lieutenant

{!syn_curl_env.md!}

### Create Tenant

!!! info
    This step is only required if the Tenant does not exist yet.

{!syn_create_tenant.md!}

### Create Cluster

{!syn_create_cluster.md!}


## Prepare Host Cluster

Create a namespace and service account on the host cluster.

```shell title="Environment Variables"
export TARGET_NAMESPACE="my-vcluster"
export TARGET_SERVICEACCOUNT="gitlab-deployer"
```

1. Create the namespace on the host cluster.
```shell
kubectl create ns ${TARGET_NAMESPACE}
```

1. Create a service account and token for the GitLab pipeline.
```shell
kubectl -n ${TARGET_NAMESPACE} create serviceaccount ${TARGET_SERVICEACCOUNT}

cat <<EOF | kubectl -n ${TARGET_NAMESPACE} apply -f -
apiVersion: v1
kind: Secret
metadata:
  annotations:
    kubernetes.io/service-account.name: ${TARGET_SERVICEACCOUNT}
  name: ${TARGET_SERVICEACCOUNT}-token
type: kubernetes.io/service-account-token
EOF
```

1. Create the rolebinding for the new service account.
```shell
cat <<EOF | kubectl -n ${TARGET_NAMESPACE} apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: ${TARGET_SERVICEACCOUNT}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: admin
subjects:
- kind: ServiceAccount
  name: ${TARGET_SERVICEACCOUNT}
EOF
```

## Compile Cluster Catalog

!!! warning
    From here on its a construction site 🤷‍♂️

Clone tenant repository to adjust some initial values.

```shell
mkdir -p inventory/classes

git clone <TENANT_REPO> inventory/classes/${TENANT_ID}

pushd inventory/classes/${TENANT_ID}
```

Setup `component-efk-provisioning`

```shell
yq eval -i ".parameters.efk_provisioning.type = \"vcluster\"" ${CLUSTER_ID}.yml

yq eval -i ".parameters.efk_provisioning.infrastructure.kubernetes.namespace = \"${TARGET_NAMESPACE}\"" ${CLUSTER_ID}.yml
```
