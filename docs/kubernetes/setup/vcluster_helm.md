# Setup vCluster

Instructions to setup a vCluster using the Helm chart.


## Prerequisites

* `kubectl`, [Install instructions](https://kubernetes.io/docs/tasks/tools/#kubectl)
* `helm`, [Install instructions](https://helm.sh/docs/intro/install/)

```shell title="Add Helm repository"
helm repo add loft https://charts.loft.sh
helm repo update
```


## Deploy the vCluster

Create a file named `vcluster.yaml` with the following content.

Choose a distribution to deploy:

=== "K3s"

    ```yaml title="vcluster.yaml"
    controlPlane:
      distro:
        k3s:
          enabled: true
    ```

=== "K0s"

    ```yaml title="vcluster.yaml"
    controlPlane:
      distro:
        k0s:
          enabled: true
    ```

The upstream Helm chart may not work if the host cluster has some hardened security settings like OpenShift.
Thus we wan't to set some sane values for such cases.

```yaml title="vcluster.yaml"
controlPlane:
  statefulSet:
    security:
      containerSecurityContext:
        runAsGroup: null
        runAsUser: null
```

If the host cluster does not support `ephemeral-storage` you may wan't to remove their resource requests and limits.

```yaml title="vcluster.yaml"
controlPlane:
  statefulSet:
    resources:
      limits:
        ephemeral-storage: null
      requests:
        ephemeral-storage: null
```

On OpenShift you may also need to overwrite some RBAC rules.

```yaml title="vcluster.yaml"
rbac:
  role:
    overwriteRules:
      - apiGroups:
          - ''
        resources:
          - configmaps
          - secrets
          - services
          - pods
          - pods/attach
          - pods/portforward
          - pods/exec
          - persistentvolumeclaims
        verbs:
          - create
          - delete
          - patch
          - update
          - get
          - list
          - watch
      - apiGroups:
          - apps
        resources:
          - statefulsets
          - replicasets
          - deployments
        verbs:
          - get
          - list
          - watch
      - apiGroups:
          - ''
        resources:
          - endpoints
          - events
          - pods/log
        verbs:
          - get
          - list
          - watch
```

This should be all settings needed for a basic installation of a vCluster.

```shell title="Deploy Helm chart"
RELEASE_NAME="vcluster"
TARGET_NAMESPACE="vcluster"
helm install \
  --atomic \
  --create-namespace \
  --namespace ${TARGET_NAMESPACE} \
  --values vcluster.yaml \
  ${RELEASE_NAME} loft/vcluster
```


## Expose the vCluster

Update the file named `vcluster.yaml` with the following content.

=== "nginx"

    ```yaml title="vcluster.yaml"
    controlPlane:
      ingress:
        enabled: true
        annotations:
          cert-manager.io/cluster-issuer: letsencrypt-production
        host: api.example.com
        spec:
          tls:
            - hosts:
                - api.example.com
              secretName: vcluster-ingress-tls
    exportKubeConfig:
      server: 'api.example.com'
    ```

=== "OpenShift"

    ```yaml title="vcluster.yaml"
    controlPlane:
      ingress:
        enabled: true
        annotations:
          cert-manager.io/cluster-issuer: letsencrypt-production
          nginx.ingress.kubernetes.io/backend-protocol: null
          nginx.ingress.kubernetes.io/ssl-passthrough: null
          nginx.ingress.kubernetes.io/ssl-redirect: null
          route.openshift.io/destination-ca-certificate-secret: secret-ca-cert
          route.openshift.io/termination: reencrypt
        host: api.example.com
        spec:
          tls:
            - hosts:
                - api.example.com
              secretName: vcluster-ingress-tls
    exportKubeConfig:
      server: 'api.example.com'
    ```
