---
title: Home
hide:
  - navigation
  - toc
---

<figure markdown>
  ![Image title](assets/images/title.png){ width="800" }
  <figcaption></figcaption>
  <h1>Tegridy Documentation Project</h1>
</figure>
